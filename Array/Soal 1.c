#include <stdio.h>

int main()
{
 int bill[20], g=0, i=1;            //tipe data yang digunakan//
 
 for(i=1;i<=20;i++){                //proses pengulangan for//
    
    printf("Masukkan Nilai: ");     //instruksi agar user menginputkan bilangan//
    scanf("%d", &bill[i]);          //scan terhadap bilangan yang diinputkan user,lalu disimpan pada array[i]//
    
    if(bill[i]>g){                  //menggunakan program if untuk mencari nilai tertinggi//
        g = bill[i];
    }
 }
 printf("Nilai Tertinggi adalah:%d", g);        //output program berupa nilai tertinggi//
 
 return 0;
}