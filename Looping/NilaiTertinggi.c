/* Fig. 5.3: fig05_03.c
    Creating a maximum of three integers */
#include <stdio.h>

int maximum( int x, int y, int z ); /* function prototype */

/* function main begins program execution */
int main ( void )
{
    int number1; /* first integer */
    int number2; /* second integer */
    int number3; /* third integer */
    
    printf( "Enter three integers: " );
    scanf( "%d%d%d", &number1, &number2, &number3 );
    
    /* number1, number 2, number 3 are arguments
       to the maximum function call */
    printf( "maximum is: %d\n", maximum( number1, number2, number3 ) );
    return 0; /* indicates successful termination */
} /* end main */

/* Function maximum definitin */
/* x, y and z are parameters */
int maximum( int x, int y, int z )
{
    int max = x; /* assume x is largest */
    if (y > max ) { /* if y is larger than max, assign y to max */
       max = y;
    } /* end if */
    
        if (z > max ) { /* if y is larger than max, assign z to max */
       max = z;
    } /* end if */
    
    return max; /* max is largest value */
} /* end function maximum */