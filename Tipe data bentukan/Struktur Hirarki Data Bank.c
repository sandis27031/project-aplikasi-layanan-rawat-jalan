#include <stdio.h>
#include <string.h>

typedef struct {
    char nama_jalan[30];
    char kota[20];
    int kode_pos;
} alamat;

typedef struct {
    char nama_depan[20];
    char nama_belakang [40];
    alamat domisili;
} pelanggan;

typedef struct{
    int no_rek;
    pelanggan pemilik_rek;
    char jenis_rek[100];
    int saldo;
}info;
    
int main(){
    int i;
    info rek[2];
    int total = 0;
    
    rek[0].no_rek = 382730;
    strcpy(rek[0].pemilik_rek.nama_depan, "Anita");
    strcpy(rek[0].pemilik_rek.nama_belakang, "Joli");
    strcpy(rek[0].jenis_rek, "Taplus");
    strcpy(rek[0].pemilik_rek.domisili.nama_jalan, "Mawar");
    strcpy(rek[0].pemilik_rek.domisili.kota, "Bandung");
    rek[0].pemilik_rek.domisili.kode_pos = 4730;
    rek[0].saldo = 534430;
    
    rek[1].no_rek = 529182;
    strcpy(rek[1].pemilik_rek.nama_depan, "Dini");
    strcpy(rek[1].pemilik_rek.nama_belakang, "Andini");
    strcpy(rek[1].jenis_rek, "Tapenas");
    strcpy(rek[1].pemilik_rek.domisili.nama_jalan, "Melati");
    strcpy(rek[1].pemilik_rek.domisili.kota, "Karawang");
    rek[1].pemilik_rek.domisili.kode_pos = 5467;
    rek[1].saldo = 837280;  
    
    rek[2].no_rek = 427918;
    strcpy(rek[2].pemilik_rek.nama_depan, "Nomi");
    strcpy(rek[2].pemilik_rek.nama_belakang, "Agustin");
    strcpy(rek[2].jenis_rek, "Taplus");
    strcpy(rek[2].pemilik_rek.domisili.nama_jalan, "Pahlawan");
    strcpy(rek[2].pemilik_rek.domisili.kota, "Bogor");
    rek[2].pemilik_rek.domisili.kode_pos = 6590;
    rek[2].saldo = 938200;
    
    printf("NOMOR REKENING\tPEMILIK\t\tJENIS REKENING\tALAMAT(KOTA,NAMA JALAN)\tKODE POS\tSALDO\n");
    for(i=0;i<3;i++){
        printf("%d\t\t%s %s\t%s\t\t%s,%s\t\t%d\t\t%d\t\n",rek[i].no_rek,rek[i].pemilik_rek.nama_depan,rek[i].pemilik_rek.nama_belakang,rek[i].jenis_rek,rek[i].pemilik_rek.domisili.kota,rek[i].pemilik_rek.domisili.nama_jalan,rek[i].pemilik_rek.domisili.kode_pos,rek[i].saldo);
	}
	
	for(i=0;i<3;i++){
		if(rek[i].saldo <= 938200){
			total = total + rek[i].saldo;
			printf("%d\n", total);
		}else{
			printf("Lalalala\n");
		}
	}
return 0;
}
