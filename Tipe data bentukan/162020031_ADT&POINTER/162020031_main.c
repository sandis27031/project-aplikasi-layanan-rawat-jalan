#include "point.c"

int main() {
	POINT X, Y, P;
	POINT P1, P2;
	float jarak;
	int newX, newY, deltaX, deltaY;
	
	
	printf("=====ADT POINT=====\n");
	P1 = makePoint(3,4);
	P2 = makePoint(-7,5);

	printPoint(P1);
	printPoint(P2);

	jarak = jarakPoint(P1, P2);
	printf("Jarak P1 dan P2 adalah %0.2f\n", jarak);

	printf("P: Kuadran %d, P2: kuadran %d\n", kuadranPoint(P1), kuadranPoint(P2));
	
	printf("%d, %d\n", getX(P1), getY(P1));
	printf("%d, %d\n", getX(P2), getY(P2));
	
	printPoint(plusPOINT(P1,P2));
	
	printf("=====Weekly Activity 6=====\n ");
	
	printf("Masukkan absis : ");
	scanf("%d", &newX);
	setX(&P, newX);
	
	printf("Masukkan ordinat : ");
	scanf("%d", &newY);	
	setY(&P, newY);
	
	P = makePoint(newX,newY);
	
	printPoint(P);
	
	printf("Hasil Pengurangan P1 dan P2 adalah "); printPoint(minusPOINT(P1,P2));
	
	printf("Jarak P ke titik (0,0) adalah %0.2f \n",jarak0(P));
	
	printf("P1 = P2  : %d \n", EQ(P1, P2));
	printf("P1 != P2 : %d \n", NEQ(P1, P2));
	
	printf("P1 < P2  : %d \n", isLowerThan(P1,P2));		
	printf("P1 > P2  : %d \n", isGreaterThan(P1,P2));
	
	printf("Titik P terletak pada sumbu X (X,0) : %d\n", isOnX(P));
	printf("Titik P terletak pada sumbu Y (0,Y) : %d\n", isOnY(P));
			
	printf("Masukkan deltaX : ");
	scanf("%d", &deltaX);
	printf("Masukkan deltaY : ");
	scanf("%d", &deltaY);
	
	geserPOINT(&P, deltaX, deltaY);
	
	printf("I.S : P terdefinisi pada titik(%d,%d)\n", P.X, P.Y);
	printf("I.F : P digeser abisnya sebesar X : %d dan ordinatnya sebesar Y : %d menjadi titik(%d,%d)", deltaX, deltaY, P.X+deltaX, P.Y+deltaY);
	
	return 0;
}
