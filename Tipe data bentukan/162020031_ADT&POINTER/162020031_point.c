#include <math.h>
#include <stdio.h>
#include "point.h"

POINT makePoint(int XX, int YY){
	POINT P;
	
	P.X = XX;
	P.Y = YY;
	
	return P;
}

void printPoint(POINT P){
	
	printf("Point = (%d, %d)\n", P.X, P.Y);
	
}

double jarakPoint(POINT P1, POINT P2){
	double jarak;
	
	jarak = sqrt(pow((P2.X-P1.X),2)+(pow((P2.Y-P1.Y),2)));
	
	return jarak;
}

int kuadranPoint(POINT P){
	
	if (P.X > 0 && P.Y > 0){
	return 1;
	} else if (P.X < 0 && P.Y > 0){
	return 2;
	} else if (P.X < 0 && P.Y < 0){
	return 3;
	} else {
	return 4;
	}
}

// Weekly Activity 6//

int getX(POINT P){
	return P.X;
}

int getY(POINT P){
	return P.Y;
}

POINT plusPOINT(POINT P1, POINT P2){
	POINT P;
	
	P.X = P1.X + P2.X;
	P.Y = P1.Y + P2.Y;
	return P;
	
}

void setX (POINT *P,int newX){	
	
	getX(*P) == newX;
		
}

void setY (POINT *P,int newY){
		
	getY(*P) == newY;
}

POINT minusPOINT(POINT P1, POINT P2){
	POINT P;
	
	P.X = P1.X - P2.X;
	P.Y = P1.Y - P2.Y;
	return P;
	
}

float jarak0(POINT P){
	float jarak;
	
	jarak = sqrt(pow((P.X),2)+(pow((P.Y),2)));
	
	return jarak;
}

int EQ(POINT P1,POINT P2){
	
	if((P1.X == P2.X) && (P1.Y == P2.Y)){
		return 1;
	}else{
		return 0;
	}
	

}

int NEQ(POINT P1,POINT P2){
	
	if((P1.X != P2.X) && (P1.Y != P2.Y)){
		return 1;
	}else{
		return 0;
	}
	
}

int isLowerThan(POINT P1,POINT P2){
	
	if(P1.X < P2.X){
		if(P1.Y < P2.Y){
			return 1;
		}else if(P1.Y > P2.Y){
			return 1;
		}else{
			return 0;
		}
	}else{
		return 0;
	}
	
}

int isGreaterThan(POINT P1,POINT P2){
	
	if(P1.X > P2.X){
		if(P1.Y > P2.Y){
			return 1;
		}else if(P1.Y < P2.Y){
			return 1;
		}else{
			return 0;
		}
	}else{
		return 0;
	}
	
}

int isOnX(POINT P){
	if(P.Y == 0 ){
		return 1;
	}else{
		return 0;
	}
}

int isOnY(POINT P){
	if(P.X == 0){
		return 1;
	}else{
		return 0;
	}
}

void geserPOINT(POINT *P,int deltaX,int deltaY){
	getX(*P) == getX(*P) + deltaX;
	getY(*P) == getY(*P) + deltaY;
}

