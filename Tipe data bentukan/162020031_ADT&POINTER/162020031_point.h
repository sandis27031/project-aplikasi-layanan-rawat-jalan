#ifndef point_h
#define point_h

typedef struct{
	int X;
	int Y;
}POINT;

POINT makePOINT(int x, int y);
void printPoint(POINT P);
double jarakPoint(POINT P1, POINT P2);
int kuadranPoint(POINT P);

int getX(POINT P);
int getY(POINT P);

POINT plusPOINT(POINT P1, POINT P2);
POINT minusPOINT(POINT P1,POINT P2);

void setX (POINT *P,int newX);
void setY (POINT *P,int newY);

float jarak0(POINT P);

int EQ(POINT P1,POINT P2);
int NEQ(POINT P1,POINT P2);

int isLowerThan(POINT P1,POINT P2);
int isGreaterThan(POINT P1,POINT P2);

int isOnX(POINT P);
int isOnY(POINT P);

void geserPOINT(POINT *P,int deltaX,int deltaY);

#endif

