#include <stdio.h>
#include <math.h>

typedef struct{
	int X;
	int Y;
}POINT;

POINT makePoint(int XX, int YY);
void printPoint(POINT P);
double jarakPoint(POINT P1, POINT P2);
int kuadranPoint(POINT P);

int main() {
	POINT P1, P2;
	double jarak;
	
	P1 = makePoint(3,4);
	P2 = makePoint(-7,5);
	
	printPoint(P1);
	printPoint(P2);
	
	jarak = jarakPoint(P1, P2);
	printf("Jarak P1 dan P2 adalah %0.2f\n", jarak);
	
	printf("P1: Kuadran %d, P2: Kuadran %d\n", kuadranPoint(P1), kuadranPoint(P2));

	return 0;
}

POINT makePoint(int XX, int YY){
	POINT P;
	
	P.X = XX;
	P.Y = YY;
	
	return P;
}

void printPoint(POINT P){
	printf("Point = (%d, %d)\n", P.X, P.Y );
}

double jarakPoint(POINT P1, POINT P2){
	double jarak;
	
	jarak = sqrt(pow((P2.X-P1.X),2)+(pow((P2.Y-P1.Y),2)));
	
	return jarak;
}

int kuadranPoint(POINT P){
	if (P.X > 0 && P.Y > 0){
	return 1;
	} else if (P.X < 0 && P.Y > 0){
	return 2;
	} else if (P.X < 0 && P.Y < 0){
	return 3;
	} else {
	return 4;
	}
}




