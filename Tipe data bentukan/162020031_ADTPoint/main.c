#include "point.c"

int main() {
	POINT P1, P2;
	float jarak;

	P1 = makePoint(3,4);
	P2 = makePoint(-7,5);

	printPoint(P1);
	printPoint(P2);

	jarak = jarakPoint(P1, P2);
	printf("Jarak P1 dan P2 adalah %0.2f\n", jarak);

	printf("P: Kuadran %d, P2: kuadran %d\n", kuadranPoint(P1), kuadranPoint(P2));

	return 0;
}
