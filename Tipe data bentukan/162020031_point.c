#include <stdio.h>
#include <math.h>

typedef struct{
	int X;
	int Y;
}POINT;


int main() {
	POINT P1, P2;
	double jarak;
	
	P1.X = 4;
	P1.Y = 3;
	
	P2.X = -7;
	P2.Y = 5;
	
	printf("P1 = (%d, %d)\n", P1.X, P1.Y);
	printf("P2 = (%d, %d)\n", P2.X, P2.Y);
	
	jarak = sqrt(pow((P2.X-P1.X),2)+(pow((P2.Y-P1.Y),2)));
	printf("Jarak antara P1 dan P2 = %0.2f\n", jarak);
	
	if (P1.X > 0 && P1.Y > 0){
		printf("P1 berada di Kuadran 1\n");
	}else if (P1.X < 0 && P1.Y > 0){
		printf("P1 berada di Kuadran 2\n");
	} else if (P1.X < 0 && P1.Y < 0){
		printf("P1 berada di Kuadran 3\n");
	} else {
		printf("P1 berada di Kuadran 4\n");
	}
	
	if (P2.X > 0 && P2.Y > 0){
		printf("P2 berada di Kuadran 1\n");
	} else if (P2.X < 0 && P2.Y > 0){
		printf("P2 berada di Kuadran 2\n");
	} else if (P2.X < 0 && P2.Y < 0){
		printf("P2 berada di Kuadran 3\n");
	} else {
		printf("P2 berada di Kuadran 4\n");
	}
	
	return 0;
}
