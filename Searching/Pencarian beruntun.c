#include <stdio.h>

int main(){
    
    //deklarasi
    int i, key, ketemu, arr[10];
    
    //input elemen array
    for(i=0;i<10;i++){
        scanf("%d", &arr[i]);
    }
    
    //input key search
    printf("input kata kunci : ");
    scanf("%d", &key);
    
    //proses pencarian
    //inisialisasi
    i = 0;
    ketemu = 0;     //INI PENTING
    
    while(ketemu == 0 && i<10){
        if(arr[i] == key){
            ketemu = 1;
        }
        i++;
    }
    
    if(ketemu == 1)
        printf("Data ditemukan pada indeks : %d\n", i-1);
    else
        printf("Key not found");
    
    return 0;
}