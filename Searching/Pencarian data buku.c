#include <stdio.h>
#include <string.h>
#define MAXCHAR 50  //maksimal karakter


typedef struct {
    char pengarang[50];
    char judul [50];
    int tahun;
}buku;

void tampilkanbuku(buku* arrbuku){
    int i, ketemu;
    char input[10];
    printf("Masukkan nama pengarang : ");
    scanf("%s", input);
    if(sizeof(arrbuku) == 0){   //pengecekan array kosong
        printf("array kosong");
    }
    else{
        //proses pencarian
        i=0;
        ketemu=0;

        while(ketemu == 0 && i < sizeof(arrbuku)){
            if(strcmp(arrbuku[i].pengarang, input) == 0){
                printf("pengarang: %s\n", arrbuku[i].pengarang);
                printf("judul: %s\n", arrbuku[i].judul);
                printf("tahun: %d\n\n", arrbuku[i].tahun);
                //ketemu = 1;
            }
            i++;   
        }
    }
    
}

int main(){
    buku arrbuku[4];
    
    strcpy(arrbuku[0].pengarang, "kaka");
    strcpy(arrbuku[0].judul, "belajar java");
    arrbuku[0].tahun = 2006;
    
    strcpy(arrbuku[1].pengarang, "sandi");
    strcpy(arrbuku[1].judul, "belajar alpro");
    arrbuku[1].tahun = 2007;
    
    strcpy(arrbuku[2].pengarang, "sandi");
    strcpy(arrbuku[2].judul, "belajar C");
    arrbuku[2].tahun = 2010;
    
    strcpy(arrbuku[3].pengarang, "agus");
    strcpy(arrbuku[3].judul, "belajar java");
    arrbuku[3].tahun = 2000;
    
    strcpy(arrbuku[4].pengarang, "carissa");
    strcpy(arrbuku[4].judul, "belajar phyton");
    arrbuku[4].tahun = 2008;
    tampilkanbuku(arrbuku);
    
    return 1;
}
